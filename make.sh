#!/bin/sh

COMMIT_HASH=`git rev-parse HEAD 2>/dev/null` 
BUILD_DATE=`(date "+%Y-%m-%d %H:%M:%S")`
TARGET=./bin/robot 
SOURCE=./main.go
#-o ${TARGET} ${SOURCE} 

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    export GOPROXY="https://goproxy.io"
    export GOPATH="/mnt/d/go:/home/smirkcat/go"
    export GO111MODULE="on"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo "skip - darwin detected"
elif [[ "$OSTYPE" == "cygwin" ]]; then
    echo "nskip - cygwin detected"
fi


go build -ldflags "-X \"main.BuildVersion=${COMMIT_HASH}\" -X \"main.BuildDate=${BUILD_DATE}\""

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	echo "moving files"
	mkdir -p /www/tronscan/linux
	cp tron /www/tronscan/linux/tron
	cp trx.toml /www/tronscan/trx.toml
	mkdir -p /home/user/tron/key_store
elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo "skip - darwin detected"
    
elif [[ "$OSTYPE" == "cygwin" ]]; then
    echo "nskip - cygwin detected"
fi
